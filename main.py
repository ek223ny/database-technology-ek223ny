import os
import mysql.connector
from mysql.connector import errorcode
import sys

cnx = mysql.connector.connect(user='root', password='root', host='127.0.0.1')

DB_NAME = 'kindergarten'

cursor = cnx.cursor()

# This code is ment to be run with MAMP. At least it works for me when I run it with MAMP.

# I've re-used some code from the first programming assignment, though the vast majority of the code is brand new.

# When you run the program it will ask you if you want to delete the database. This is just to make it easier for me
# and for you as testers to reset the database to its original state whenever you'd like to.

def read_file(file_path):       # Reused code from an earlier project.
    """
    Returns the contained text from the given file as a list.
    With every line being a value in the list.
    """
    fulltext = []
    with open(file_path, "r") as file:
        for line in file:                       
            fulltext += [line]
    return fulltext

def read_data(table):
    path = os.getcwd()
    path += f"/data/{table}.csv"
    MM_list = read_file(path)           # Calls the function to read the csv files.
    matrix = []
    for line in MM_list:
        line = line.strip("\n")
        newlist = line.split(",")  
        matrix.append(newlist)
    return matrix

def create_database(cursor, DB_NAME):
    try:
        cursor.execute(f"CREATE DATABASE {DB_NAME} DEFAULT CHARACTER SET 'utf8'")
    except mysql.connector.Error as err:                                                        # The create_database function is Ilir's code from workshop 1.
        print(f"Failed creating database: {err}")

def add_tables_kindergarten(cursor):
    print("Adding tables...")
    print("Creating department table...")
    creation_command = "CREATE TABLE departments (d_name varchar(20) NOT NULL, PRIMARY KEY (d_name))"
    try:
        cursor.execute(creation_command) 
    except mysql.connector.Error as err:
            if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
                print("Table 'departments' already exists.")
            else:
                print(err.msg)

    print("Creating child table...")
    creation_command = "CREATE TABLE children (" \
    "c_ssn bigint NOT NULL," \
    "c_name varchar(50) NOT NULL," \
    "c_age tinyint NOT NULL," \
    "c_dpt varchar(20) NOT NULL," \
    "PRIMARY KEY (c_ssn)," \
    "FOREIGN KEY (c_dpt) " \
    "REFERENCES departments(d_name) " \
    "ON UPDATE CASCADE ON DELETE RESTRICT)"             # ON DELETE set to restrict in order to make sure that every child and teacher must be assigned to a department.
    try:
        cursor.execute(creation_command) 
    except mysql.connector.Error as err:
            if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
                print("Table 'children' already exists.")
            else:
                print(err.msg)

    print("Creating teacher table...")
    creation_command = "CREATE TABLE teachers (" \
    "t_id int NOT NULL AUTO_INCREMENT," \
    "t_name varchar(50) NOT NULL," \
    "t_phone varchar(20) NOT NULL," \
    "t_dpt varchar(20) NOT NULL," \
    "PRIMARY KEY (t_id)," \
    "FOREIGN KEY (t_dpt) " \
    "REFERENCES departments(d_name) " \
    "ON UPDATE CASCADE ON DELETE RESTRICT)"
    try:
        cursor.execute(creation_command)  
    except mysql.connector.Error as err:
            if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
                print("Table 'teachers' already exists.")
            else:
                print(err.msg)

    print("Creating guardian table...")
    creation_command = "CREATE TABLE guardians (" \
    "g_ssn bigint NOT NULL," \
    "g_name varchar(50) NOT NULL," \
    "g_phone varchar(20) NOT NULL," \
    "PRIMARY KEY (g_ssn))"
    try:
        cursor.execute(creation_command)   
    except mysql.connector.Error as err:
            if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
                print("Table 'guardians' already exists.")
            else:
                print(err.msg)

    print("Creating associative table ChildrenGuardians...")
    creation_command = "CREATE TABLE cg (" \
    "c_ssn bigint NOT NULL," \
    "g_ssn bigint NOT NULL," \
    "PRIMARY KEY (c_ssn, g_ssn)," \
    "FOREIGN KEY (c_ssn) " \
    "REFERENCES Children(c_ssn) " \
    "ON UPDATE CASCADE ON DELETE CASCADE," \
    "FOREIGN KEY (g_ssn) " \
    "REFERENCES Guardians(g_ssn) " \
    "ON UPDATE CASCADE ON DELETE CASCADE)"
    try:
        cursor.execute(creation_command)  
    except mysql.connector.Error as err:
            if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
                print("Table 'cg' already exists.")
            else:
                print(err.msg)

    print("Creating allergy table...")
    creation_command = "CREATE TABLE allergies (" \
    "ac_ssn bigint NOT NULL," \
    "a_name varchar(20) NOT NULL," \
    "PRIMARY KEY (a_name, ac_ssn)," \
    "FOREIGN KEY (ac_ssn) " \
    "REFERENCES Children(c_ssn) " \
    "ON UPDATE CASCADE ON DELETE CASCADE)"
    try:
        cursor.execute(creation_command)   
    except mysql.connector.Error as err:
            if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
                print("Table 'allergies' already exists.")
            else:
                print(err.msg)
    print("Done.")

def populate_tables(cursor): 
    print("Populating tables...")

    data = read_data("departments")
    attributes = data.pop(0)
    for tuples in data:
        command = "INSERT INTO departments (" \
            f"{attributes[0]}) " \
            f"VALUES ({tuples[0]})"
        try:
            cursor.execute(command)           
        except mysql.connector.Error as err:
            print("\n1 Tuple failed to be added to table 'departments'. Reason:", err.msg) 

    data = read_data("children")
    attributes = data.pop(0)
    for tuples in data:
        command = "INSERT INTO children (" \
            f"{attributes[0]}, {attributes[1]}, {attributes[2]}, {attributes[3]}) " \
            f"VALUES ({tuples[0]},{tuples[1]},{tuples[2]},{tuples[3]})"
        try:
            cursor.execute(command)           
        except mysql.connector.Error as err:
            print("\n1 Tuple failed to be added to table 'children'. Reason:", err.msg)      

    data = read_data("allergies")
    attributes = data.pop(0)
    for tuples in data:
        command = "INSERT INTO allergies (" \
            f"{attributes[0]}, {attributes[1]}) " \
            f"VALUES ({tuples[0]},{tuples[1]})"
        try:
            cursor.execute(command)           
        except mysql.connector.Error as err:
            print("\n1 Tuple failed to be added to table 'allergies'. Reason:", err.msg)

    data = read_data("guardians")
    attributes = data.pop(0)
    for tuples in data:
        command = "INSERT INTO guardians (" \
            f"{attributes[0]}, {attributes[1]}, {attributes[2]}) " \
            f"VALUES ({tuples[0]},{tuples[1]},{tuples[2]})"
        try:
            cursor.execute(command)           
        except mysql.connector.Error as err:
            print("\n1 Tuple failed to be added to table 'guardians'. Reason:", err.msg)

    data = read_data("teachers")
    attributes = data.pop(0)
    for tuples in data:
        command = "INSERT INTO teachers (" \
            f"{attributes[0]}, {attributes[1]}, {attributes[2]}) " \
            f"VALUES ({tuples[0]},{tuples[1]},{tuples[2]})"
        try:
            cursor.execute(command)           
        except mysql.connector.Error as err:
            print("\n1 Tuple failed to be added to table 'teachers'. Reason:", err.msg)            

    data = read_data("cg")
    attributes = data.pop(0)
    for tuples in data:
        command = "INSERT INTO cg (" \
            f"{attributes[0]}, {attributes[1]}) " \
            f"VALUES ({tuples[0]},{tuples[1]})"
        try:
            cursor.execute(command)           
        except mysql.connector.Error as err:
            print("\n1 Tuple failed to be added to table 'cg'. Reason:", err.msg)      
    print("Done.")

def add_views(cursor):
    print("Creating view for children and allergies.")      # Choosing to make a children and allergy view, since allergies are important in this database.
    try:
        cursor.execute("CREATE VIEW ca AS SELECT LPAD(children.c_ssn, 10, 0) AS c_ssn, children.c_name, allergies.a_name FROM children JOIN allergies ON children.c_ssn = allergies.ac_ssn")
        print("Done.")
    except mysql.connector.Error as err:
        print(err.msg)

def what_allergies(cursor):
    while True:
        c_ssn = input("What's the child's ten digit SSN? ")
        if c_ssn.isnumeric() and len(c_ssn) == 10:
            break
        else:
            print("Please enter a valid 10 digit SSN.")
    try:
        cursor.execute(f"SELECT a_name AS 'Allergy' FROM ca WHERE c_ssn = {c_ssn}")         # Makes use of the view.
        querylist = cursor.fetchall()
        if len(querylist) == 0:
            print(f"There is no child with SSN {c_ssn} that have any allergies.")
        else:
            print("\n{:^20s}".format(cursor.column_names[0]))
            for value in querylist:
                print("\n{:^20s}".format(str(value[0])))
    except mysql.connector.Error as err:
        print(err.msg)
    input("Press enter to return to menu. \n")

def avg_age_by_dpt(cursor): # Realistically would not exist in the real database. Here in order to fulfill the requirement of aggregate functions.
    try:            
        cursor.execute("SELECT c_dpt AS 'Department name', AVG(c_age) AS 'Average age' FROM children " \
        "GROUP BY c_dpt")
        querylist = cursor.fetchall()
        print("\n{:^20s} {:^20s}".format(cursor.column_names[0], cursor.column_names[1]))
        for value in querylist:
            print("\n{:^20s} {:^20s}".format(str(value[0]), str(value[1])))
    except mysql.connector.Error as err:
        print(err.msg)
    input("Press enter to return to menu. \n")

def children_amount_by_dpt(cursor):
    try:
        cursor.execute("SELECT c_dpt, COUNT(c_ssn) AS 'Children amount' FROM children GROUP BY c_dpt")
        querylist = cursor.fetchall()
        print("\n{:^20s} {:^20s}".format(cursor.column_names[0], cursor.column_names[1]))
        for value in querylist:
            print("\n{:^20s} {:^20s}".format(str(value[0]), str(value[1])))
    except mysql.connector.Error as err:
        print(err.msg)
    input("Press enter to return to menu. \n")

def who_has_allergy(cursor):
    a_name = input("What's the name of the allergy? ")
    try:
        cursor.execute(f"SELECT c_ssn AS 'SSN', c_name AS 'Name' FROM ca WHERE a_name = '{a_name}'") # Also makes use of the view.
        querylist = cursor.fetchall()
        if len(querylist) == 0:
            print(f"There are no children with the allergy {a_name}.")
        else:
            print("\n{:^20s} {:^20s}".format(cursor.column_names[0], cursor.column_names[1]))
            for value in querylist:
                print("\n{:^20s} {:^20s}".format(str(value[0]), str(value[1])))
    except mysql.connector.Error as err:
        print(err.msg)
    input("Press enter to return to menu. \n")

def allergy_by_dpt(cursor):
    d_name = input("Which department? ")
    try:            # Makes use of the view.
        cursor.execute(f"SELECT ca.c_ssn AS 'SSN', ca.c_name AS 'Name', ca.a_name AS 'Allergy' FROM ca, children WHERE children.c_ssn = ca.c_ssn AND children.c_dpt = '{d_name}' ORDER BY ca.a_name")
        querylist = cursor.fetchall()
        if len(querylist) == 0:
            print(f"There are no children with allergies in department {d_name}.")
        else:
            print("\n{:^20s} {:^20s} {:^20s}".format(cursor.column_names[0], cursor.column_names[1], cursor.column_names[2]))
            for value in querylist:
                print("\n{:^20s} {:^20s} {:^20s}".format(str(value[0]), str(value[1]), format(str(value[2]))))
    except mysql.connector.Error as err:
        print(err.msg)
    input("Press enter to return to menu. \n")

def who_is_guardian(cursor):
    while True:
        c_ssn = input("What's the child's ten digit SSN? ")
        if c_ssn.isnumeric() and len(c_ssn) == 10:
            break
        else:
            print("Please enter a valid 10 digit SSN.")
    try:
        cursor.execute(f"SELECT LPAD(guardians.g_ssn, 10, 0) AS g_ssn, guardians.g_name, guardians.g_phone FROM guardians, cg WHERE cg.c_ssn = {c_ssn} AND cg.g_ssn = guardians.g_ssn")
        querylist = cursor.fetchall()
        if len(querylist) == 0:
            print("No child with currently assigned guardians with given SSN in database.")
        else:
            print("\n{:^20s} {:^20s} {:^20s}".format(cursor.column_names[0], cursor.column_names[1], cursor.column_names[2]))
            for value in querylist:
                print("\n{:^20s} {:^20s} {:^20s}".format(str(value[0]), str(value[1]), format(str(value[2]))))
    except mysql.connector.Error as err:
        print(err.msg)
    input("Press enter to return to menu. \n")

def children_by_teacher(cursor):
    t_id = input("What's the teacher's ID? ")
    try:
        cursor.execute(f"SELECT LPAD(children.c_ssn, 10, 0) AS c_ssn, children.c_name, children.c_age FROM children, teachers WHERE children.c_dpt = teachers.t_dpt AND teachers.t_id = {t_id}")
        querylist = cursor.fetchall()
        print("\n{:^20s} {:^20s} {:^20s}".format(cursor.column_names[0], cursor.column_names[1], cursor.column_names[2]))
        for value in querylist:
            print("\n{:^20s} {:^20s} {:^20s}".format(str(value[0]), str(value[1]), format(str(value[2]))))
    except mysql.connector.Error as err:
        print(err.msg)
    input("Press enter to return to menu. \n")

# Made these into separate menus due to the sheer amount of queries,
# and in order to make it more user friendly.
def show_table_menu(cursor):
    print("----------------------------------Tables-----------------------------------")
    print("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
    print("|  0.  | Back to main menu.                                              |")
    print("--------------------------------------------------------------------------")
    print("|  1.  | Children.                                                       |")
    print("|  2.  | Teachers.                                                       |")
    print("|  3.  | Guardians.                                                      |")
    print("|  4.  | Departments.                                                    |")
    print("|  5.  | Allergies.                                                      |")
    print("|  6.  | Associative table for children and guardians.                   |")
    print("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
    while True:
        try:
            query_num = int(input("\n"))
        except:
            continue            # No need to make individual functions when the queries are so small and require no input.
        if query_num == 0:
            break
        elif query_num == 1:
            try:        
                cursor.execute("SELECT LPAD(c_ssn, 10, 0) AS 'c_ssn', c_name, c_age, c_dpt FROM children")          # The choice of using LPAD is since SSN's can start with zeroes.
                querylist = cursor.fetchall()
                print("\n{:^20s} {:^20s} {:^20s} {:^20s}".format(cursor.column_names[0], cursor.column_names[1], cursor.column_names[2], cursor.column_names[3]))
                for value in querylist:
                    print("\n{:^20s} {:^20s} {:^20s} {:^20s}".format(str(value[0]), str(value[1]), str(value[2]), str(value[3])))
            except mysql.connector.Error as err:
                print(err.msg)
            input("\n Press enter to go back to menu. \n")
            break
        elif query_num == 2:
            try:
                cursor.execute("SELECT * FROM teachers")
                querylist = cursor.fetchall()
                print("\n{:^20s} {:^20s} {:^20s} {:^20s}".format(cursor.column_names[0], cursor.column_names[1], cursor.column_names[2], cursor.column_names[3]))
                for value in querylist:
                    print("\n{:^20s} {:^20s} {:^20s} {:^20s}".format(str(value[0]), str(value[1]), str(value[2]), str(value[3])))
            except mysql.connector.Error as err:
                print(err.msg)
            input("\n Press enter to go back to menu. \n")
            break
        elif query_num == 3:
            try:
                cursor.execute("SELECT LPAD(g_ssn, 10, 0) AS 'g_ssn', g_name, g_phone FROM guardians")
                querylist = cursor.fetchall()
                print("\n{:^20s} {:^20s} {:^20s}".format(cursor.column_names[0], cursor.column_names[1], cursor.column_names[2]))
                for value in querylist:
                    print("\n{:^20s} {:^20s} {:^20s}".format(str(value[0]), str(value[1]), str(value[2])))
            except mysql.connector.Error as err:
                print(err.msg)
            input("\n Press enter to go back to menu. \n")
            break
        elif query_num == 4:
            try:
                cursor.execute("SELECT * FROM departments")
                querylist = cursor.fetchall()
                print("\n{:^20s}".format(cursor.column_names[0]))
                for value in querylist:
                    print("\n{:^20s}".format(str(value[0])))
            except mysql.connector.Error as err:
                print(err.msg)
            input("\n Press enter to go back to menu. \n")
            break
        elif query_num == 5:
            try:
                cursor.execute("SELECT LPAD(ac_ssn, 10, 0) AS 'ac_ssn', a_name FROM allergies ORDER BY a_name")
                querylist = cursor.fetchall()
                print("\n{:^20s} {:^20s}".format(cursor.column_names[0], cursor.column_names[1]))
                for value in querylist:
                    print("\n{:^20s} {:^20s}".format(str(value[0]), str(value[1])))
            except mysql.connector.Error as err:
                print(err.msg)
            input("\n Press enter to go back to menu. \n")
            break
        elif query_num == 6:
            try:
                cursor.execute("SELECT LPAD(g_ssn, 10, 0) AS 'g_ssn', LPAD(c_ssn, 10, 0) AS 'c_ssn' FROM cg")
                querylist = cursor.fetchall()
                print("\n{:^20s} {:^20s}".format(cursor.column_names[0], cursor.column_names[1]))
                for value in querylist:
                    print("\n{:^20s} {:^20s}".format(str(value[0]), str(value[1])))
            except mysql.connector.Error as err:
                print(err.msg)
            input("\n Press enter to go back to menu. \n")
            break

def data_addition_menu(cursor):
    print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
    print("| 0. | Back to main menu.                                               |")
    print("-------------------------------------------------------------------------")
    print("| 1. | New child.                                                       |")
    print("| 2. | New allergy for existing child.                                  |")
    print("| 3. | New teacher.                                                     |")
    print("| 4. | New guardian.                                                    |")
    print("| 5. | New child-guardian relation for existing child and guardian.     |")
    print("| 6. | New department.                                                  |")
    print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
    while True:
        try:
            query_num = int(input("\n"))
        except:
            continue
        if query_num == 0:
            break
        elif query_num == 1:
            addsubquery1(cursor)
            input("Press enter to return to menu. \n")
            break
        elif query_num == 2:
            addsubquery2(cursor)
            input("Press enter to return to menu. \n")
            break
        elif query_num == 3:
            addsubquery3(cursor)
            input("Press enter to return to menu. \n")
            break
        elif query_num == 4:
            addsubquery4(cursor)
            input("Press enter to return to menu. \n")
            break
        elif query_num == 5:
            addsubquery5(cursor)
            input("Press enter to return to menu. \n")
            break
        elif query_num == 6:
            addsubquery6(cursor)
            input("Press enter to return to menu. \n")
            break

def data_deletion_menu(cursor):
    print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
    print("| 0. | Back to main menu.                                               |")
    print("-------------------------------------------------------------------------")
    print("| 1. | Delete child.                                                    |")
    print("| 2. | Delete allergy for existing child.                               |")
    print("| 3. | Delete teacher.                                                  |")
    print("| 4. | Delete guardian.                                                 |")
    print("| 5. | Delete child-guardian relation for existing child and guardian.  |")
    print("| 6. | Delete department.                                               |")
    print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
    while True:
        try:
            query_num = int(input("\n"))
        except:
            continue
        if query_num == 0:
            break
        elif query_num == 1:
            delsubquery1(cursor)
            input("Press enter to return to menu. \n")
            break
        elif query_num == 2:
            delsubquery2(cursor)
            input("Press enter to return to menu. \n")
            break
        elif query_num == 3:
            delsubquery3(cursor)
            input("Press enter to return to menu. \n")
            break
        elif query_num == 4:
            delsubquery4(cursor)
            input("Press enter to return to menu. \n")
            break
        elif query_num == 5:
            delsubquery5(cursor)
            input("Press enter to return to menu. \n")
            break
        elif query_num == 6:
            delsubquery6(cursor)
            input("Press enter to return to menu. \n")
            break

def data_update_menu(cursor):
    print("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
    print("|  0.  | Back to main menu.                                              |")
    print("--------------------------------------------------------------------------")
    print("|  1.  | Update department name.                                         |") # Only did the essential update queries due to time constraints.
    print("|  2.  | Update child department.                                        |")
    print("|  3.  | Update teacher department.                                      |")
    print("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
    while True:
        try:
            query_num = int(input("\n"))
        except:
            continue
        if query_num == 0:
            return
        elif query_num == 1:
            dpt_update_name(cursor)
            input("Press enter to return to menu. \n")
            break
        elif query_num == 2:
            dpt_update_child(cursor)
            input("Press enter to return to menu. \n")
            break
        elif query_num == 3:
            dpt_update_teacher(cursor)
            input("Press enter to return to menu. \n")
            break


def dpt_update_name(cursor):
    d_name = input("Enter the name of the department you wish to update, alternatively enter '0' to cancel. ")
    if d_name == '0':
        return
    d_name = f"'{d_name}'"
    while True:
        d_name_new = input("Enter the new name of the department. (No longer than 20 symbols.) ")
        if len(d_name_new) > 20:
            print("That name is too long, please try again.")
            continue
        d_name_new = f"'{d_name_new}'"
        break


    command = "UPDATE departments " \
    f"SET d_name = {d_name_new} " \
    f"WHERE d_name = {d_name}" 
    
    try:
        cursor.execute(command)
        print(f"Department {d_name} changed to {d_name_new}.")
    except mysql.connector.Error as err:
        print("\nDepartment failed to be updated. Reason:", err.msg) 

def dpt_update_child(cursor):
    while True:
        try:
            c_ssn = input("Enter the child's 10 digit SSN, alternatively, enter 0 to cancel. ")
            int(c_ssn)
            if c_ssn == '0':
                return
            if len(c_ssn) != 10:
                print("Please make sure to enter an SSN that's 10 digits long.")
                continue
        except ValueError:
            print("Please make sure to enter a number.")
            continue
        break

    c_dpt = input("Enter the department that this child is to be assigned to.  ")
    c_dpt = f"'{c_dpt}'"
    
    command = "UPDATE children " \
    f"SET c_dpt = {c_dpt} " \
    f"WHERE c_ssn = {int(c_ssn)}" 
    
    try:
        cursor.execute(command)
        print(f"Department for child with SSN {c_ssn} changed to {c_dpt}.")
    except mysql.connector.Error as err:
        print("\nDepartment failed to be updated. Reason:", err.msg) 

def dpt_update_teacher(cursor):
    while True:
        try:
            t_id = input("Enter the teacher's ID, alternatively, enter 0 to cancel. ")
            int(t_id)
            if t_id == '0':
                return
        except ValueError:
            print("Please make sure to enter a number.")
            continue
        break

    t_dpt = input("Enter the department that this teacher is to be assigned to.  ")
    t_dpt = f"'{t_dpt}'"
    
    command = "UPDATE teachers " \
    f"SET t_dpt = {t_dpt} " \
    f"WHERE t_id = {int(t_id)}" 
    
    try:
        cursor.execute(command)
        print(f"Department for teacher with ID {t_id} changed to {t_dpt}.")
    except mysql.connector.Error as err:
        print("\nDepartment failed to be updated. Reason:", err.msg) 

def addsubquery1(cursor):
    c_ssn, c_name, c_age, c_dpt = "", "", "", ""
    while True:
        if c_ssn == "":
            try:
                c_ssn = input("Input the ten digit SSN of the child, alternatively, input 0 to cancel.")
                int(c_ssn)
                if c_ssn == 0:
                    return
                if len(str(c_ssn)) != 10:
                    print("Please make sure to enter an SSN that's 10 digits long.")
                    c_ssn = ""
                    continue
            except ValueError:
                print("Please make sure to enter a number.")
                c_ssn = ""
                continue
        if c_name == "":
            c_name = input("Name of the child. (No longer than 50 symbols.)")
            if len(c_name) > 50:
                print("Name cannot be longer than 50 symbols.")
                c_name = ""
                continue
            c_name = f"'{c_name}'"
        if c_age == "":
            try:
                c_age = int(input("The one digit age of the child."))
                if c_age > 9:
                    print("Age may only be one digit.")
                    c_age = ""
                    continue
            except ValueError:
                print("Age must be a number.")
                c_age = ""
                continue
        if c_dpt == "":
            c_dpt = input("The department assigned to the child.")
            c_dpt = f"'{c_dpt}'"
        command = "INSERT INTO children (" \
        "c_ssn, c_name, c_age, c_dpt) " \
        f"VALUES ({int(c_ssn)},{c_name},{c_age},{c_dpt})"
        try:
            cursor.execute(command)
            print(f"Child added with attributes {c_ssn},{c_name},{c_age},{c_dpt}")
            break
        except mysql.connector.Error as err:
            print("\nNew child failed to be added. Reason:", err.msg) 
            break

def addsubquery2(cursor):
    while True:
        try:
            ac_ssn = int(input("Enter the child's 10 digit SSN, alternatively, enter 0 to cancel. "))
            if ac_ssn == 0:
                return
        except ValueError:
            print("Please enter a number")
            continue
        break
    
    while True:
        a_name = input("Enter a single allergy. (No longer than 20 symbols long).")
        if len(a_name) > 20:
            print("No more than 20 symbols long.")
            continue
        a_name = f"'{a_name}'"
        break
    command = "INSERT INTO allergies (" \
    "ac_ssn, a_name) " \
    f"VALUES ({ac_ssn},{a_name})"
    try:
        cursor.execute(command)   
        print(f"Allergy added with attributes {ac_ssn},{a_name}")        
    except mysql.connector.Error as err:
        print("\nAllergy failed to be added. Reason:", err, err.msg)

def addsubquery3(cursor):
    while True:
        t_name = input("Enter the teacher's name. (No longer than 50 symbols.) Alternatively, enter 0 to cancel. ")
        if t_name == '0':
            return
        if len(t_name) > 50:
            print("That name is too long, please enter a name no longer than 50 symbols.")
            continue
        t_name = f"'{t_name}'"
        break
    while True:
        t_phone = input("Enter one of the teacher's phone numbers. (No longer than 20 symbols long).")
        if len(t_phone) > 20:
            print("No more than 20 symbols long.")
            continue
        t_phone = f"'{t_phone}'"
        break
    t_dpt = input("Enter the department the teacher is assigned to.")
    t_dpt = f"'{t_dpt}'"

    
    command = "INSERT INTO teachers (" \
        "t_name, t_phone, t_dpt) " \
        f"VALUES ({t_name},{t_phone},{t_dpt})"
    try:
        cursor.execute(command)
        print(f"Teacher added with attributes {t_name},{t_phone},{t_dpt}")
    except mysql.connector.Error as err:
        print("\nNew teacher failed to be added. Reason:", err.msg) 

def addsubquery4(cursor):
    while True:
        try:
            g_ssn = input("Enter the guardian's 10 digit SSN, alternatively, enter 0 to cancel. ")
            int(g_ssn)
            if g_ssn == '0':
                return
            if len(g_ssn) != 10:
                print("Please make sure to enter an SSN that's 10 digits long.")
                continue
        except ValueError:
            print("Please make sure to enter a number.")
            continue
        break

    while True:
        g_name = input("Enter the guardian's name. (No longer than 50 symbols.) ")
        if len(g_name) > 50:
            print("That name is too long. Please enter a name no longer than 50 symbols.")
            continue
        g_name = f"'{g_name}'"
        break     

    while True:
        g_phone = input("Enter one of the guardians's phone numbers. (No longer than 20 symbols long).")
        if len(g_phone) > 20:
            print("No more than 20 symbols long.")
            continue
        g_phone = f"'{g_phone}'"
        break
    
    command = "INSERT INTO guardians (" \
        "g_ssn, g_name, g_phone) " \
        f"VALUES ({int(g_ssn)},{g_name},{g_phone})"
    try:
        cursor.execute(command)
        print(f"Guardian added with attributes {g_ssn},{g_name},{g_phone}")
    except mysql.connector.Error as err:
        print("\nNew guardian failed to be added. Reason:", err.msg) 

def addsubquery5(cursor):
    while True:
        try:
            g_ssn = input("Enter the guardian's 10 digit SSN, alternatively, enter 0 to cancel. ")
            int(g_ssn)
            if g_ssn == '0':
                return
            if len(g_ssn) != 10:
                print("Please make sure to enter an SSN that's 10 digits long.")
                continue
        except ValueError:
            print("Please make sure to enter a number.")
            continue
        break

    while True:
        try:
            c_ssn = input("Enter the child's 10 digit SSN. ")
            int(c_ssn)
            if c_ssn == '0':
                return
            if len(c_ssn) != 10:
                print("Please make sure to enter an SSN that's 10 digits long.")
                continue
        except ValueError:
            print("Please make sure to enter a number.")
            continue
        break

    command = "INSERT INTO cg (" \
        "g_ssn, c_ssn) " \
        f"VALUES ({int(g_ssn)},{int(c_ssn)})"
    try:
        cursor.execute(command)
        print(f"Guardian-Child relation added with attributes {g_ssn},{c_ssn}")
    except mysql.connector.Error as err:
        print("\nNew guardian-child relation failed to be added. Reason:", err.msg) 

def addsubquery6(cursor):
    while True:
        d_name = input("Input the name of the new department. (No longer than 20 symbols.), alternatively, enter 0 to cancel.  ")
        if d_name == '0':
            return
        if len(d_name) > 20:
            print("That name is too long. Make sure the department name has 20 symbols or less.")
            continue
        d_name = f"'{d_name}'"
        break
        
    command = "INSERT INTO departments (" \
        "d_name) " \
        f"VALUES ({d_name})"
    try:
        cursor.execute(command)
        print(f"Department added with attribute {d_name}.")
    except mysql.connector.Error as err:
        print("\nNew department failed to be added. Reason:", err.msg) 

def delsubquery1(cursor):
    while True:
        try:
            c_ssn = input("Enter the child's 10 digit SSN, alternatively, enter 0 to cancel. ")
            int(c_ssn)
            if c_ssn == '0':
                return
            if len(c_ssn) != 10:
                print("Please make sure to enter an SSN that's 10 digits long.")
                continue
        except ValueError:
            print("Please make sure to enter a number.")
            continue
        break

    command = "DELETE FROM children " \
    f"WHERE c_ssn = {int(c_ssn)} " 
    
    try:
        cursor.execute(command)
        print(f"Child deleted with SSN {c_ssn}.")
    except mysql.connector.Error as err:
        print("\nChild failed to be deleted. Reason:", err.msg) 

def delsubquery2(cursor):
    while True:
        try:
            ac_ssn = input("Enter the child's 10 digit SSN, alternatively, enter 0 to cancel. ")
            int(ac_ssn)
            if ac_ssn == '0':
                return
            if len(ac_ssn) != 10:
                print("Please make sure to enter an SSN that's 10 digits long.")
                continue
        except ValueError:
            print("Please make sure to enter a number.")
            continue
        break

    a_name = input("Enter an allergy that this child has.  ")
    a_name = f"'{a_name}'"
    
    command = "DELETE FROM allergies " \
    f"WHERE ac_ssn = {int(ac_ssn)} AND a_name = {a_name}" 
    
    try:
        cursor.execute(command)
        print(f"Allergy deleted with attributes {ac_ssn}, {a_name}.")
    except mysql.connector.Error as err:
        print("\nAllergy failed to be deleted. Reason:", err.msg) 

def delsubquery3(cursor):
    while True:
        try:
            t_id = input("Enter the teacher's ID, alternatively, enter 0 to cancel. ")
            int(t_id)
            if t_id == '0':
                return
        except ValueError:
            print("Please make sure to enter a number.")
            continue
        break
    
    command = "DELETE FROM teachers " \
    f"WHERE t_id = {int(t_id)}" 

    try:
        cursor.execute(command)
        print(f"Teacher deleted with ID number {t_id}.")
    except mysql.connector.Error as err:
        print("\nTeacher failed to be deleted. Reason:", err.msg) 

def delsubquery4(cursor):
    while True:
        try:
            g_ssn = input("Enter the guardian's 10 digit SSN, alternatively, enter 0 to cancel. ")
            int(g_ssn)
            if g_ssn == '0':
                return
            if len(g_ssn) != 10:
                print("Please make sure to enter an SSN that's 10 digits long.")
                continue
        except ValueError:
            print("Please make sure to enter a number.")
            continue
        break

    command = "DELETE FROM guardians " \
    f"WHERE g_ssn = {int(g_ssn)} " 
    
    try:
        cursor.execute(command)
        print(f"Guardian deleted with SSN {g_ssn}.")
    except mysql.connector.Error as err:
        print("\nGuardian failed to be deleted. Reason:", err.msg) 

def delsubquery5(cursor):
    while True:
        try:
            g_ssn = input("Enter the guardian's 10 digit SSN, alternatively, enter 0 to cancel. ")
            int(g_ssn)
            if g_ssn == '0':
                return
            if len(g_ssn) != 10:
                print("Please make sure to enter an SSN that's 10 digits long.")
                continue
        except ValueError:
            print("Please make sure to enter a number.")
            continue
        break

    while True:
        try:
            c_ssn = input("Enter the child's 10 digit SSN ")
            int(c_ssn)
            if c_ssn == '0':
                return
            if len(c_ssn) != 10:
                print("Please make sure to enter an SSN that's 10 digits long.")
                continue
        except ValueError:
            print("Please make sure to enter a number.")
            continue
        break

    command = "DELETE FROM cg " \
    f"WHERE g_ssn = {int(g_ssn)} AND c_ssn = {int(c_ssn)}" 
    
    try:
        cursor.execute(command)
        print(f"Guardian-child relation deleted with guardian SSN, respectively child SSN {g_ssn},{c_ssn}.")
    except mysql.connector.Error as err:
        print("\nGuardian-child relation failed to be deleted. Reason:", err.msg)  

def delsubquery6(cursor):
    while True:
        d_name = input("Enter the department name, alternatively, enter 0 to cancel. ")
        if d_name == '0':
            return
        d_name = f"'{d_name}'"
        break
    
    command = "DELETE FROM departments " \
    f"WHERE d_name = {d_name}" 
    try:
        cursor.execute(command)
        print(f"Department deleted with name {d_name}.")
    except mysql.connector.Error as err:
        if err.errno == 1451:
            print("\nDepartment cannot be deleted as teachers or children are still assigned to it.")   # Added description since this will be the most common exception raised.
        else:
            print(f"\nDepartment failed to be deleted. Reason: {err.msg}")

data = input("Drop database? If yes, enter '1', otherwise just hit enter.")
if data == "1":
    try:
        cursor.execute(f"DROP DATABASE {DB_NAME}")
        print("Database dropped.")
    except mysql.connector.Error as err:
        print(err.msg)
    input("Press enter to continue. ")

try:
    cursor.execute(f"USE {DB_NAME}")
except mysql.connector.Error as err:            
    if err.errno == errorcode.ER_BAD_DB_ERROR:
        print(f"Database {DB_NAME} does not exist.")
        create_database(cursor, DB_NAME)
        print(f"Database {DB_NAME} created successfully.")   
        cursor.execute(f"USE {DB_NAME}")                      
        add_tables_kindergarten(cursor)
        populate_tables(cursor)
        add_views(cursor)
    else:
        print(err)

while True:
    print("-----------------------------------Menu-----------------------------------")
    print("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
    print("|  0.  | Quit.                                                           |")   # Added quit command to not force you to kill the terminal to stop the program.
    print("|  1.  | What's the allergies of the child with the given SSN?           |")
    print("|  2.  | Average age by department.                                      |")
    print("|  3.  | Children amount by department.                                  |")
    print("|  4.  | Which children have the given allergy?                          |")
    print("|  5.  | Which allergies are present in children of a certain department?|")
    print("|  6.  | Show the guardians of a certain child?                          |")
    print("|  7.  | What children are a certain teacher responsible for?            |")
    print("|  8.  | Show a table in its entirety.                                   |")
    print("--------------------------------------------------------------------------")
    print("|  9.  | Add tuples to the database.                                     |")
    print("|  10. | Delete tuples from the database.                                |")
    print("|  11. | Update departments.                                             |")
    print("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
    while True:
        try:
            query_num = int(input("\n"))
        except:
            continue
        if query_num == 0:
            sys.exit()
        elif query_num == 1:
            what_allergies(cursor)
            break
        elif query_num == 2:
            avg_age_by_dpt(cursor)
            break
        elif query_num == 3:
            children_amount_by_dpt(cursor)
            break
        elif query_num == 4:
            who_has_allergy(cursor)
            break
        elif query_num == 5:
            allergy_by_dpt(cursor)
            break
        elif query_num == 6:
            who_is_guardian(cursor)
            break
        elif query_num == 7:
            children_by_teacher(cursor)
            break
        elif query_num == 8:
            show_table_menu(cursor)
            break
        elif query_num == 9:
            data_addition_menu(cursor)
            break
        elif query_num == 10:
            data_deletion_menu(cursor)
            break
        elif query_num == 11:
            data_update_menu(cursor)
            break
